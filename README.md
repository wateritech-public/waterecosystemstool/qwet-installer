## **QWET** 

QWET is an interface that operates the coupled open source hydrodynamic-aquatic ecosystem model GOTM-FABM-WET through QGIS.
QWET is NOT backwards-compatible and do NOT support WET-projects that have been setup through the WET-QGIS2 interface.

**QWET 3.4.2 New version (29. Jun 2022)**
The latest version (3.4.2) integrates a new WET model (the engine behind) and has been beta-tested to support QGIS 3.22 Stable Release version.


**QWET 3.4.1 New version (22. Feb 2021)**
The latest version (3.4.1) is upgraded to support the QGIS 3.16.16 Stable Release version.
That means support for Python 3.9.5. 


**QWET 3.3.0 New version (26. nov 2021)**
The latest version (3.3.0) is upgraded to support the QGIS 3.16.x stable release version. 
The new QWET features a Calibration Helper Tool and the skeleton for a QWET Check Tool. 
The QWET Check Tool will be finalized in a future version of QWET.

